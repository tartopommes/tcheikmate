//Seulement des emits comme ce n'est pas côté server
(function(){
   let loc = 'http://localhost:8100'
   const socket = io.connect(loc);
   let playerName = "Anonymous";
   let skillRating = 100;
   let isSearching = false;
   let isInGame = false;


   socket.on('serverTell',(message)=>{
      document.getElementById('chat_history').innerHTML += "</br>"
      document.getElementById('chat_history').innerHTML += "[SERVER] : " +  message
   });


   socket.on('chatReceive',(from,message)=>{
      document.getElementById('chat_history').innerHTML += "</br>"
      document.getElementById('chat_history').innerHTML += from + " : " + message
   });


   socket.on('onMatchFound',(message)=>{
      document.getElementById('chat_history').innerHTML += "</br>"
      document.getElementById('chat_history').innerHTML += "[SERVER] : " + message
   });

   socket.on('retrieveScoreBoard',(scoreboard)=>{
      table = document.getElementById('table_score')
      for ( let elem of scoreboard ){
         let row = table.insertRow(table.rows.length)
         row.insertCell(0).innerHTML = elem.name;
         row.insertCell(1).innerHTML = elem.sr + " sr";
         row.insertCell(2).innerHTML = elem.score + " pts";
      }
   });

   socket.on('SendClientToGame',()=>{
      //Window (super var en js)
      //window.location.href =
      window.location.assign(loc+"/game")
   });

   //Faire un if player isSearching et un !isSearching (modifiant la valeur du
   // bouton et de la page)

   //Fonction appelée lorsque le client appuye sur le bouton SearchGame
   function OnButtonSearch(){
      if(!isSearching){ // Si il n'était pas en recherche
         isSearching = true;
         socket.emit('playerSearching',playerName,skillRating,isSearching);
         document.getElementById('buttonSearch').value = "Searching..."
      }
      else{ // Si il était en recherche
         isSearching = false;
         socket.emit('playerSearching',playerName,skillRating,isSearching);
         document.getElementById('buttonSearch').value = "Search Game !"
      }
   }


   //Appelé lorsque le client envoie un message dans le chat
   function OnChatSend(){
      let message = document.getElementById('message_send').value//On récupère le contenu du form
      document.getElementById('message_send').value = ""// On reset le text du form
      socket.emit("chatSend",playerName,message)  // On le passe au serveur
   }                                              // Puis le serveur le renvoi à tous les client

   //
   function OnButtonSubmitProfil(){
      let newNick = document.getElementById('nom_profil').value;
      let newSr = document.getElementById('sr').value;
      if(typeof(newSr) === typeof(0) || newNick != ""){
               document.getElementById('nom_profil').value = "";
               document.getElementById('sr').value = "";
               playerName = newNick;
               skillRating = newSr;
               document.getElementById('navbarDropdown').innerHTML = "Playing as : " + newNick;
      }

   }
   document.getElementById('buttonSearch').addEventListener('click',OnButtonSearch);
   document.getElementById('buttonSubmitProfil').addEventListener('click',OnButtonSubmitProfil);

   //____________________________________________________________________
   //  addEventListener pour le chat
   document.getElementById('Chat_enter').addEventListener('click',OnChatSend);
   let keyInputOnChat = document.getElementById("message_send");
   keyInputOnChat.addEventListener("keyup", function(event) {
   if (event.keyCode === 13) {
      event.preventDefault();
      document.getElementById("Chat_enter").click();
   }
   });

})()
