let plateau = document.getElementById("map_jeu");
let contexte = plateau.getContext('2d'); //le contexte va représenter ici toutes les modifications d'affichage

let style = plateau.currentStyle || window.getComputedStyle(plateau);
let margeX = 0; //10 marge automatique si non précisé
let margeY = (window.innerHeight * (2.5/100) + 50); //10 marge automatique si non précisé
let plateauLargeur = plateau.width;
let plateauHauteur = plateau.height;
let trigger = false; //false = rien n'est séléctionné

//variables utiles pour le préchargement d'images
let load = new Array;
let pièces = new Array;
pièces = ['../img/pion_noir.png','../img/pion_blanc.png','../img/tour_blanche.png','../img/tour_noir.png','../img/fou_blanc.png','../img/fou_noir.png','../img/cavalier_blanc.png','../img/cavalier_noir.png','../img/reine_blanc.png','../img/reine_noir.png','../img/roi_blanc.png','../img/roi_noir.png'];

function affichageDamierVierge(){
    let plateauLargeur = plateau.width;
    let plateauHauteur = plateau.height;
    let largeur;
    let hauteur;
    contexte.clearRect(0,0,plateauLargeur,plateauHauteur);
    //coloration des cases noires et blaches
    let cases;
    let lignes=0;
    largeur =0;
    hauteur =0;
    for (cases=1;cases<=8*8;cases++){
    
        if(largeur == plateauLargeur){   largeur =0;    hauteur +=plateauHauteur/8; lignes++; }
    
        contexte.beginPath();
        if((cases+lignes)%2==1){
            contexte.fillStyle="#3B371C";


        }
        else{
            contexte.fillStyle="#FCEB70";
        }
        contexte.rect(largeur,hauteur,largeur+plateauLargeur/8,hauteur+plateauHauteur/8);
        contexte.fill();
        contexte.closePath();
        //début test affichage déplacement
        contexte.beginPath();
        contexte.strokeStyle="red";
        contexte.lineWidth = 3;
        //haut à gauche
        contexte.moveTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(25/100)*(plateauHauteur/8));
        contexte.moveTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(25/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        
        //gaut à doite
        contexte.moveTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(25/100)*(plateauHauteur/8));
        contexte.moveTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(75/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        
        //bas à gauche
        contexte.moveTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(25/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.moveTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(75/100)*(plateauHauteur/8));
        
        //bas à droite
        contexte.moveTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(75/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.moveTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(75/100)*(plateauHauteur/8));
        contexte.stroke();
        
        
        //fin test
        largeur+=(plateauLargeur/8);
        contexte.lineWidth = 1;
    }

    //dessin des cases
    for ( largeur=0; largeur<=plateauLargeur; largeur+=(plateauLargeur/8) ){ //de haut en bas
        contexte.strokeStyle="black";
        contexte.lineWidth = 2;
        contexte.moveTo(largeur,0);
        contexte.lineTo(largeur,plateauHauteur);
        contexte.stroke();
    }
    for ( hauteur=0; hauteur<=plateauHauteur; hauteur+=(plateauHauteur/8) ){ //de gauche en droite
        contexte.fillStyle="black";
        contexte.lineWidth = 2;
        contexte.moveTo(0,hauteur);
        contexte.lineTo(plateauLargeur,hauteur);
        contexte.stroke();
    }
    return;
}




function affichagePieces(tab){//ici tab est le tableau avec la position de toutes les pièces
    for (var j=0; j<8; j++){
        for(var u=0; u<8; u++){
            
            switch(tab[(j*8)+u]){
                case 'P':
                    affichagePion(j,u,'white');
                    break;
                case 'p':
                    affichagePion(j,u,'black');
                    break;
                case 'R':
                    affichageTour(j,u,'white');
                    break;
                case 'r':
                    affichageTour(j,u,'black');
                    break;
                case 'B':
                    affichageFou(j,u,'white');
                    break;
                case 'b':
                    affichageFou(j,u,'black');
                    break;
                case 'N':
                    affichageCavalier(j,u,'white');
                    break;
                case 'n':
                    affichageCavalier(j,u,'black');
                    break;
                case 'Q':
                    affichageReine(j,u,'white');
                    break;
                case 'q':
                    affichageReine(j,u,'black');
                    break;
                case 'K':
                    affichageRoi(j,u,'white');
                    break;
                case 'k':
                    affichageRoi(j,u,'black');
                    break;
            }
        }
    }
}

function affichageOnClick(tab,trigger,caseX,caseY){
    if(tab[caseX][caseY]!=0){
        trigger=true;
    }
    else{
        trigger=false;
    }
    
    
}

plateau.addEventListener('click', function posCaseOnClick(e){
    caseX = e.clientX ;
    caseY = e.clientY ;
    
    caseX = (caseX - margeX) / ((plateauLargeur/8));
    caseY = (caseY-margeY) / ((plateauHauteur/8) );
    
    caseX = Math.trunc(caseX);
    caseY = Math.trunc(caseY);

    //pour la partie jeu mettez ici votre fonction qui nécessite la position du clic (ou utlisez une fonction pour juste récupérer la position)
    return(affichageOnClick(tab,trigger,caseX,caseY));
})


function prechargementImage(pièces){
    let i=0;
    for(i=0; i < pièces.length ; i++){
        let piece = new Image();
        piece.src = pièces[i];
        load[i]=piece;
    }

    return(load);
}

function affichagePion(caseHauteur,caseLargeur, couleur){

    if(couleur == 'white') {
        contexte.drawImage(load[0],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[1],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageTour(caseHauteur,caseLargeur, couleur){
    if(couleur == 'white') {
        contexte.drawImage(load[2],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[3],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageFou(caseHauteur,caseLargeur, couleur){
    if(couleur == 'white') {
        contexte.drawImage(load[4],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[5],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageCavalier(caseHauteur,caseLargeur, couleur){
if(couleur == 'white') {
        contexte.drawImage(load[6],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[7],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageReine(caseHauteur,caseLargeur, couleur){
if(couleur == 'white') {
        contexte.drawImage(load[8],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[9],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageRoi(caseHauteur,caseLargeur, couleur){
if(couleur == 'white') {
        contexte.drawImage(load[10],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[11],(plateauLargeur/8)*(caseLargeur+0.15), (plateauHauteur/8)*caseHauteur);
    }
}

prechargementImage(pièces);
affichageDamierVierge();
affichagePieces( Board.fetFENboard() );
