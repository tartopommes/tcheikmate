//Seulement des emits comme ce n'est pas côté server
(function(){
   let loc = 'http://localhost:8100'
   const socket = io.connect(loc);

   let amIWhite = false; //Si le joueur joue les blancs
   let isMyTurn = false; //Si c'est son tour

   let playerName = undefined
   let sr = undefined

   let bufferSelectedSquare = undefined;
   //buffer permettant de savoir
   //quelle est le dernier pion selectionné du joueur
   //car le premier clique client est la pièce à selectionnée et
   //le second clique est l'endroit où la déplacer

   let movesList; //Moves possibles de chaques pions
   let fenBoard;  //board à afficher



   /*----------------------------------------------------------------------------*/
   /*-------------------PARTIE DEFINITION DES SOCKET.ON------------------------*/
   /*----------------------------------------------------------------------------*/

   socket.on('serverTell',(message)=>{
      document.getElementById('chat_history').innerHTML += "</br>"
      document.getElementById('chat_history').innerHTML += "[SERVER] : " +  message
   });

   socket.on('chatReceive',(from,message)=>{
      document.getElementById('chat_history').innerHTML += "</br>"
      document.getElementById('chat_history').innerHTML += from + " : " + message
   });

   //Appelé lorsque le client envoie un message dans le chat
   function OnChatSend(){
      let message = document.getElementById('message_send').value//On récupère le contenu du form
      document.getElementById('message_send').value = ""// On reset le text du form
      socket.emit("chatSend",playerName,message)  // On le passe au serveur
   }                                              // Puis le serveur le renvoi à tous les client

   socket.on('SendClientToMainMenu',()=>{
      window.location.assign(loc)
   });

   socket.on('GAME_SERVtoCLIENT_initGame',(bool,name,sr)=>{
      amIWhite = bool;
      if(amIWhite){document.getElementById("inGame").value = "You are White"}
      else{document.getElementById("inGame").value = "You are Black"}
      playerName = name;
      sr = sr
   });


    socket.on('GAME_SERVtoCLIENT_sendWhoCanPlay',(color)=>{
        if(amIWhite && color =="white"){
            isMyTurn = true;
            console.log("This is my turn !")
        }
        else if(!amIWhite && color == "black"){
            isMyTurn = true;
            console.log("This is my turn !")
        }
        else{
            isMyTurn = false;
            console.log("This is NOT my turn !")
        }

    });



    socket.on('GAME_SERVtoCLIENT_sendBoardToDisplay',(board, history)=>{

        fenBoard = board;
        clearPlateau();
        affichagePieces(fenBoard);

        console.log(history);
        if (history != undefined) {
            let htmlHistory = document.getElementById(`promote`);
            htmlHistory.innerHTML = ``;

            // create elements
            let htable = document.createElement("table");
            let hthead = document.createElement("thead");
            let htbody = document.createElement("tbody");
            let hmhtr = document.createElement("tr");
            let hshtr = document.createElement("tr");
            let hmhth = document.createElement("th");
            let hshNbth = document.createElement("th");
            let hshPlayerth = document.createElement("th");
            let hshPMovedth = document.createElement("th");
            let hshMoveth = document.createElement("th");
            let hshpCaughtth = document.createElement("th");
            
            // set elements
            htable.style.textAlign = `center`;
            hmhth.setAttribute(`colspan`, `5`);
            hmhth.innerHTML = `History`;
            hshNbth.innerHTML  = `#`;
            hshPlayerth.innerHTML  = `Player`;
            hshPMovedth.innerHTML  = `Piece moved`;
            hshMoveth.innerHTML    = `Move`;
            hshpCaughtth.innerHTML = `Caught piece`;

            // append elements
            hmhtr.appendChild(hmhth);
            hshtr.appendChild(hshNbth);
            hshtr.appendChild(hshPlayerth);
            hshtr.appendChild(hshPMovedth);
            hshtr.appendChild(hshMoveth);
            hshtr.appendChild(hshpCaughtth);
            hthead.appendChild(hmhtr);
            hthead.appendChild(hshtr);

            for (let move of history) {
                let caughtPiece = (move[5] == `.`) ? `None` : move[5];

                //create elements
                let hbtr = document.createElement("tr");
                let hbNbtd = document.createElement("td");
                let hbPlayertd = document.createElement("td");
                let hbPMovedtd = document.createElement("td");
                let hbMovetd = document.createElement("td");
                let hbpCaughttd = document.createElement("td");

                //set elements
                hbNbtd.innerHTML  = `${move[0]}`;
                hbPlayertd.innerHTML  = `${move[1]}`;
                hbPMovedtd.innerHTML  = `${move[2]}`;
                hbMovetd.innerHTML    = `${move[3]} --> ${move[4]}`;
                hbpCaughttd.innerHTML = `${caughtPiece}`;

                // append elements
                hbtr.appendChild(hbNbtd);
                hbtr.appendChild(hbPlayertd);
                hbtr.appendChild(hbPMovedtd);
                hbtr.appendChild(hbMovetd);
                hbtr.appendChild(hbpCaughttd);
                hthead.appendChild(hbtr);
            }

            // append elements
            htable.append(hthead);
            htable.append(htbody);
            htmlHistory.appendChild(htable);
        }
    });


    socket.on('GAME_SERVtoCLIENT_sendMoveList',(list)=>{
        movesList = list;
    });


    socket.on('GAME_SERVtoCLIENT_sendGameOver',(isStalemate,concernedPlayer)=>{


        document.getElementById('chat_history').innerHTML += "</br>"
        if(isStalemate){
            document.getElementById('chat_history').innerHTML += "[SERVER] : " + concernedPlayer + " loses because he is in Stalemate !";

        }
        else{
            document.getElementById('chat_history').innerHTML += "[SERVER] : "  + concernedPlayer + " wins because his opponent is in Checkmate !";
        }
        document.getElementById('chat_history').innerHTML += "</br>";
        document.getElementById('chat_history').innerHTML += "[SERVER] : Returning to the main menu soon...";
    });


    socket.on('GAME_SERVtoCLIENT_sendPromotionAvailable',()=>{
        //
        // CALL PROMOTION
        //
    });
    //On vient réassigner dès le début les sockets aux joueurs
    //Et démarrer la partie
    socket.emit("reassignSockets");



   /*----------------------------------------------------------------------------*/
   /*---------------------------PARTIE AFFICHAGE---------------------------------*/
   /*----------------------------------------------------------------------------*/
   let plateau = document.getElementById("map_jeu");
   let contexte = plateau.getContext('2d'); //le contexte va représenter ici toutes les modifications d'affichage

   let margeX = 660; //10 marge automatique si non précisé
   let margeY = 217; //10 marge automatique si non précisé
   let plateauLargeur = plateau.width;
   let plateauHauteur = plateau.height;
   let trigger = false; //false = rien n'est séléctionné

    let load = new Array;
    let tabPieceSrc = new Array;
    tabPieceSrc = ['../img/pion_noir.png','../img/pion_blanc.png','../img/tour_blanche.png','../img/tour_noir.png','../img/fou_blanc.png','../img/fou_noir.png','../img/cavalier_blanc.png','../img/cavalier_noir.png','../img/reine_blanc.png','../img/reine_noir.png','../img/roi_blanc.png','../img/roi_noir.png'];
    prechargementImage(tabPieceSrc);


   function affichageDamierVierge(){
       let plateauLargeur = plateau.width;
       let plateauHauteur = plateau.height;
       let largeur;
       let hauteur;

       //coloration des cases noires et blaches
       let cases;
       let lignes=0;
       largeur =0;
       hauteur =0;
       for (cases=1;cases<=8*8;cases+=1){

           if(largeur == plateauLargeur){   largeur =0;    hauteur +=plateauHauteur/8; lignes++; }

           contexte.beginPath();
           if((cases+lignes)%2==1){
               contexte.fillStyle="#3B371C";
           }
           else{
               contexte.fillStyle="#FCEB70";
           }
           contexte.rect(largeur,hauteur,largeur+plateauLargeur/8,hauteur+plateauHauteur/8);
           contexte.fill();
           contexte.closePath();
           largeur+=(plateauLargeur/8);
       }

       //dessin des cases
       for ( largeur=0; largeur<=plateauLargeur; largeur+=(plateauLargeur/8) ){ //de haut en bas
           contexte.strokeStyle="black";
           contexte.lineWidth = 2;
           contexte.moveTo(largeur,0);
           contexte.lineTo(largeur,plateauHauteur);
           contexte.stroke();
       }
       for ( hauteur=0; hauteur<=plateauHauteur; hauteur+=(plateauHauteur/8) ){ //de gauche en droite
           contexte.strokeStyle="black";
           contexte.lineWidth = 2;
           contexte.moveTo(0,hauteur);
           contexte.lineTo(plateauLargeur,hauteur);
           contexte.stroke();
       }
       return;
   }


    function affichagePieces(chaineFen){//ici tab est le tableau avec la position de toutes les pièces
        let tab = new Array();
        let nombreTab=0;
        if (typeof(chaineFen) !== typeof("string")){
          return
        }
        //-----------//

        for (let i=0; i<=chaineFen.length ;i++){
           let charFen;
           charFen = chaineFen.charAt(i);
            //on détecte si dans la fen le caractère est un chiffre
            if(charFen == '1' || charFen=='2' || charFen=='3'|| charFen=='4'|| charFen=='5'|| charFen=='6'|| charFen=='7'|| charFen=='8'){
                charFenInt = parseInt(charFen);
                for (let u=0; u < charFenInt ; u++){ tab[nombreTab+u]='0'; }
                nombreTab += charFenInt;
            }
            else if(charFen != "/"){
                tab[nombreTab] = charFen;
                nombreTab++;
            }
        }
        for (let j=0; j<8; j++){
            for(let u=0; u<8; u++){

                switch(tab[(j*8)+u]){
                    case 'P':
                        affichagePion(j,u,'white');
                        break;
                    case 'p':
                        affichagePion(j,u,'black');
                        break;
                    case 'R':
                        affichageTour(j,u,'white');
                        break;
                    case 'r':
                        affichageTour(j,u,'black');
                        break;
                    case 'B':
                        affichageFou(j,u,'white');
                        break;
                    case 'b':
                        affichageFou(j,u,'black');
                        break;
                    case 'N':
                        affichageCavalier(j,u,'white');
                        break;
                    case 'n':
                        affichageCavalier(j,u,'black');
                        break;
                    case 'Q':
                        affichageReine(j,u,'white');
                        break;
                    case 'q':
                        affichageReine(j,u,'black');
                        break;
                    case 'K':
                        affichageRoi(j,u,'white');
                        break;
                    case 'k':
                        affichageRoi(j,u,'black');
                        break;
                }
            }
        }
    }

    function prechargementImage(pièces){
    let i=0;
    for(i=0; i < pièces.length ; i++){
        let piece = new Image();
        piece.src = pièces[i];
        load[i]=piece;
    }

    return(load);
    }

   function clearPlateau(){
      contexte.clearRect(0,0,plateauHauteur,plateauLargeur);
      affichageDamierVierge()
   }


   function affichagecaseDéplacement(caseX,caseY){
        let hauteur = caseY * (plateauHauteur/8);
        let largeur = caseX * (plateauLargeur/8);
        contexte.beginPath();
        /*      il faut mettre une condition pour vérifier si la case de déplacement contient une pièce adverse (et changer la couleur du coup) */
        //contexte.strokeStyle="red";
        contexte.strokeStyle="cyan";
        contexte.lineWidth = 3;
        //haut à gauche
        contexte.moveTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(25/100)*(plateauHauteur/8));
        contexte.moveTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(25/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));

        //gaut à doite
        contexte.moveTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(25/100)*(plateauHauteur/8));
        contexte.moveTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(75/100)*(plateauLargeur/8),hauteur+(5/100)*(plateauHauteur/8));

        //bas à gauche
        contexte.moveTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(25/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.moveTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(5/100)*(plateauLargeur/8),hauteur+(75/100)*(plateauHauteur/8));

        //bas à droite
        contexte.moveTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(75/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.moveTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(95/100)*(plateauHauteur/8));
        contexte.lineTo(largeur+(95/100)*(plateauLargeur/8),hauteur+(75/100)*(plateauHauteur/8));
        contexte.stroke();

   }
function affichagePion(caseHauteur,caseLargeur, couleur){

    if(couleur == 'white') {
        contexte.drawImage(load[1],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[0],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageTour(caseHauteur,caseLargeur, couleur){
    if(couleur == 'white') {
        contexte.drawImage(load[2],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[3],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageFou(caseHauteur,caseLargeur, couleur){
    if(couleur == 'white') {
        contexte.drawImage(load[4],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[5],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageCavalier(caseHauteur,caseLargeur, couleur){
if(couleur == 'white') {
        contexte.drawImage(load[6],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[7],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageReine(caseHauteur,caseLargeur, couleur){
if(couleur == 'white') {
        contexte.drawImage(load[8],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[9],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
}
function affichageRoi(caseHauteur,caseLargeur, couleur){
if(couleur == 'white') {
        contexte.drawImage(load[10],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
    else{
        contexte.drawImage(load[11],(plateauLargeur/8)*(caseLargeur), (plateauHauteur/8)*caseHauteur);
    }
}
   affichageDamierVierge();




     /* Fonction concernant l'affichage des promotions d'un pion*/
    let affichagePromo = document.getElementById("promo");
    let promoContext = affichagePromo.getContext('2d');
    function clearPromote(){
        promoContext.clearRect(0,0,plateauLargeur,plateauHauteur);
    }
    function affichagePromote(color){
       /*récupération des pièces possibles valeurs mises au hasard !*/
       let reineRestant=1;
       let fouRestant=2;
       let tourRestant=0;
       let cavalierRestant=8;
       promoContext.beginPath();


       promoContext.fillStyle = "#990033";
       promoContext.rect(plateauLargeur*(25/100),plateauHauteur*(25/100),plateauLargeur*(75/100),plateauHauteur*(40/100));
       promoContext.fill();

       promoContext.fillStyle = "black";
       promoContext.font = "20px Arial";
       promoContext.fillText("Cliquez sur la pièce que vous voulez utiliser pour remplacer votre pion",plateauLargeur*(26.5/100),plateauHauteur*(30/100));

        if(color == "black"){
            promoContext.drawImage(load[9],plateauLargeur*(35/100),plateauHauteur*(50/100));
            promoContext.drawImage(load[5],plateauLargeur*(50/100),plateauHauteur*(50/100));
            promoContext.drawImage(load[3],plateauLargeur*(65/100),plateauHauteur*(50/100));
            promoContext.drawImage(load[7],plateauLargeur*(80/100),plateauHauteur*(50/100));

        }
       if(color == "white"){
           promoContext.drawImage(load[8],plateauLargeur*(35/100),plateauHauteur*(50/100));
           promoContext.drawImage(load[4],plateauLargeur*(50/100),plateauHauteur*(50/100));
           promoContext.drawImage(load[2],plateauLargeur*(65/100),plateauHauteur*(50/100));
           promoContext.drawImage(load[6],plateauLargeur*(80/100),plateauHauteur*(50/100));
       }
       promoContext.fillStyle = "black";
       promoContext.font = "20px Arial";
       promoContext.fillText( reineRestant.toString() ,plateauLargeur*(39/100),plateauHauteur*(61/100));
       promoContext.fillText( fouRestant.toString() ,plateauLargeur*(54/100),plateauHauteur*(61/100));
       promoContext.fillText( tourRestant.toString() ,plateauLargeur*(69/100),plateauHauteur*(61/100));
       promoContext.fillText( cavalierRestant.toString() ,plateauLargeur*(84/100),plateauHauteur*(61/100));
       promoContext.closePath();

        affichagePromo.addEventListener('click', function clickPromote(e){
            posX = e.pageX;
            posY = e.pageY;
            if( posY>= 450 && posY<= 540){
                if(posX >=325 && posX <=380 && reineRestant>0){ //clic sur la reine
                        console.log("reine");
                        //ajout de la fonction promotion de engine
                        clearPromote();
                        affichagePromo.removeEventListener('click', clickPromote);
                   }
                if(posX >=460 && posX <=520 && fouRestant>0){ //clic sur le fou
                        console.log("fou");

                        clearPromote();
                        affichagePromo.removeEventListener('click', clickPromote);
                   }
                if(posX >=590 && posX <=650 && tourRestant>0){ //clic sur la tour
                        console.log("tour");

                        clearPromote();
                        affichagePromo.removeEventListener('click', clickPromote);
                   }
                if(posX >=715 && posX <=780 && cavalierRestant>0){ //clic sur le cavalier
                        console.log("cavalier");

                        clearPromote();
                        affichagePromo.removeEventListener('click', clickPromote);
                   }
            }
        });

   }

   //____________________________________________________________________
   //On ajoute le addEventListener du plateau,
   //Des vérications coté client sont réalisée ici
   //pour éviter de flood de le serveur


/*____________________________________________________________________*/
/*______________________Partie INPUT CLIENT____________________________*/
/*____________________________________________________________________*/

   //Utilisé dans le plateau.addEventListener
   // Pour retrouver l'index d'une case de movesList
      function indexOfItem(array, item) {
         let index = undefined
          for (var i = 0; i < array.length; i++) {
              if (array[i][0] == item) {
                  index = i   // Found it
              }
          }
          return index;
      }


    let reset = document.getElementById("bouttonReset");
    reset.addEventListener('click',function(){
        contexte.clearRect(0,0,plateauLargeur,plateauHauteur);
        affichageDamierVierge();
        console.log("ok");
        console.log(fenBoard);
            affichagePieces(fenBoard);
   });

   plateau.addEventListener('click', function posCaseOnClick(e){


       //Si c'est au tour du client
       if(isMyTurn){
          caseX = e.pageX;
          caseY = e.pageY;

          caseX = (caseX - margeX) / ((plateauLargeur/8));
          caseY = (caseY - margeY) / ((plateauHauteur/8) );

          caseX = Math.trunc(caseX);
          caseY = Math.trunc(caseY);

          //Si la case n'est pas vide (on cherche si la case selectionnée
          // existe dans le tableau movesList)
          if((movesList.some(row => row.includes(caseY*8 + caseX)))){

            bufferSelectedSquare = caseY*8 + caseX;
            //On doit convertir car le serveur ne compte pas les coordonnées
            //avec des tuples mais avec un int allant de 0 à 63

               let index = indexOfItem(movesList, bufferSelectedSquare)

               // On vient venir afficher tout les mouvements possible de la
               //pièce selectionnée

               clearPlateau()
               affichagePieces(fenBoard)
               for(elem of movesList[index][1]){
                  y = elem >>3;
                  x = elem & 7;
                  affichagecaseDéplacement(x,y);
               }

          }
          //Sinon (si on sélectionne un case vide ou sans déplacement)
          else{
            let squareToMoveTo = caseY*8 + caseX
            let index = indexOfItem(movesList, bufferSelectedSquare)

            //Si on reclique sur la case déjà selectionnée
            if (squareToMoveTo === bufferSelectedSquare){
               bufferSelectedSquare = undefined //On déselectionne la case
                clearPlateau()
                affichagePieces(fenBoard)

            }
            //Si la case de destination se trouve dans les moves possibles
            else if (movesList[index][1].includes(squareToMoveTo)){
               socket.emit("GAME_CLIENTtoSERV_sendMoveMade",bufferSelectedSquare,squareToMoveTo) //On envoie au serv l'input client
               bufferSelectedSquare = undefined
            }
          }
       }
       //pour la partie jeu mettez ici votre fonction qui nécessite la position du clic (ou utlisez une fonction pour juste récupérer la position)
       //return(affichageOnClick(tab,trigger,caseX,caseY));
       return
   })





   //____________________________________________________________________
   //  addEventListener pour le chat
   document.getElementById('Chat_enter').addEventListener('click',OnChatSend);
   let keyInputOnChat = document.getElementById("message_send");
   keyInputOnChat.addEventListener("keyup", function(event) {
   if (event.keyCode === 13) {
      event.preventDefault();
      document.getElementById("Chat_enter").click();
   }
   });

})()
