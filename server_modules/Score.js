//Cette classe représente un joueur,
// un joueur est doté d'un nom, d'un skillRating et d'un socket attribué
//Il a aussi 2 booléen pour savoir si il est en recherche ou en jeu

class Score{
   constructor(name,sr,score){
      this.name    = name;
      this.sr      = sr;
      this.score   = score;

   }
}

module.exports = Score;
