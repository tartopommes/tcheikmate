
//Cette variable est en dehors des exports, elle sera
//utilisée par plusieurs fonctions, elle reste uniquement définie dans
//ce module.
//let this.listOfPlayers = [];  //Cette liste va venir référencer tout les joueurs actuellement en queue


class Queue{
   constructor(){
      this.listOfPlayers = []
   }
   printSearchInfo(playerName,skillRating){
      console.log('Player',playerName,' is searching for a match with a skill rating of ',skillRating);
   }

   //Cette fonction ajoute un objet PlayerServerSide à la liste des joueur en queue
   addInWaitQueue(player){
      this.listOfPlayers.push(player);
   }

   //Cette fonction enleve un joueur de la liste de ceux qui attendent en queue
   removeFromWaitQueue(socketId){
      this.listOfPlayers = this.listOfPlayers.filter(obj => obj.socket.id =! socketId); //On vient filtrer dans
      //la liste tout ceux qui ont id correspond à celui du joueur se retirant
   }

   //nom entier : matchmakingSearchSuitableOpponent
   matchmakingSSO(player){
      let sr = player.skillRating;
      let id = player.socket.id

      let maxDiffSrValue = 120 //Cette variable va définir la différence maximale initale entre deux joueurs
      let maxIterationSr = 5 //Cette variable va définir combien de fois on va parcourir toute la liste
      //de joueur en queue avant d'arrêter
      let stepCount = 20 //A chaque itération de la liste, on va ajouter cette valeur à maxDiffSrValue
      //pour élagir les recherches afin que la game soit la plus balanced possible


      for (let i = 0; i < maxIterationSr; i++){
         for (let queuedPlayer of this.listOfPlayers){ //On parcours la liste de tout les joueurs
            if(queuedPlayer.socket.id !== id){ //Si ce n'est pas sois même
               let diff = queuedPlayer.skillRating - sr
               console.log(diff)
               if ( (diff < maxDiffSrValue) && (diff > -maxDiffSrValue)  ){
                  //Si l'adversaire rencontre les conditions requises
                  return(queuedPlayer);
               }
            }

         }
         maxDiffSrValue += stepCount;
      }
      return(undefined);
   }
}

module.exports = Queue;
