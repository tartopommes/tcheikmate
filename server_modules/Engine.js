const Board   = require('./Board');
class Engine {
    /**
     * INTERFACE TO USE SERVER'S DATA
     */

    //____________________________________________________________________________________________________________________________________________
    constructor() {
        this.chessboard = new Board();
    }


    //============================================================================================================================================
    //                                                      SEND DATA TO CLIENT
    //                                             (Call by the server, to send to the client)
    //============================================================================================================================================


    //____________________________________________________________________________________________________________________________________________
    getFen() {
        /**
         * FEN: string method to set a board
         * output = { FEN: string, moves: { [int, [int, ...], ... ], ... } to client.
         */

        return this.chessboard.getFENBoard();
    }

    //____________________________________________________________________________________________________________________________________________
    getMovesList() {
        /**
         * Array of move object
         *  moves = {
         *      [actualPos0, [ newPos00, newPos01 ]], // [pawn acutal position, [possible destinations, ...]]
         *      [actualPos1, [ newPos10, newPos11 ]]
         *  }
         */

        return this.chessboard.getMoveList();
    }

    //____________________________________________________________________________________________________________________________________________
    getHistory() {
        /**
         * Return history to display to client
         */

        let history = new Array();
        let size = this.chessboard.moveHistory.length - 1;

        if (size >= 0) {
            for (let it = size; it > size - 10 && it > -1; it--) {
                let move = this.chessboard.moveHistory[it]
                let oldPos = String.fromCharCode(97 + this.chessboard.getCol(move[0])) + (8 - this.chessboard.getRow(move[0]));
                let newPos = String.fromCharCode(97 + this.chessboard.getCol(move[1])) + (8 - this.chessboard.getRow(move[1]));

                history.push([
                    it + 1,        // move nb
                    move[2].color, // player who made the move
                    move[2].name,  // moved piece
                    oldPos,        // oldPos
                    newPos,        // newPos
                    move[3].name   // caughed peice
                ]);
            }
        }
        
        return history;
    }

    //____________________________________________________________________________________________________________________________________________
    isPromotionAvailable() {
        /**
         * Si la dernère pièce bougée est un pion
         *  Si elle s'est déplacée sur le spawn enemie
         *   return true;
         */

        return this.chessboard.canBePromoted;
    }

    //____________________________________________________________________________________________________________________________________________
    isGameOver() {
        /**
         * If players played more than 50 moves, return true,
         * If a player win, return his color,
         * Else, it's a nul match, return undefined (it's a stalemate),
         * Else, is the game isn't over, return false.
         */

        return this.chessboard.isGameOver()
    }


    //============================================================================================================================================
    //                                                      GET DATA FROM CLIENT
    //                                             (Call by the client, to send to the server)
    //============================================================================================================================================

    //____________________________________________________________________________________________________________________________________________
    setUserMove(oldPos, newPos) {
        /* Move a piece from selected side.
           'input' is like : { initPosition, newPosition } */

        let movesList = this.chessboard.getMoveList();
        let found = movesList.find(move => move[0] == oldPos && move[1].find(pos => pos == newPos));

        if (this.chessboard.canBePromoted) // Security
            this.chessboard.canBePromoted = false;

        if (found != undefined) // If the move is in list (it's that we can do it).
            if (!this.chessboard.makeMove(oldPos, newPos)) // If doMove() return False, it's that the king is in check.
                console.log(`Erreur : Roi en echec`);
            else
                console.log(`move from ${oldPos} to ${newPos} ok`);
        else
            console.log(`Erreur : Deplacement impossible`);
    }


    //____________________________________________________________________________________________________________________________________________
    setPromotion(input) {
        /** Get a promotion for a PAWN who can be promoted
         *  'input' is the type of piece.
         *  this.chessboard.canBePromoted will take false
         */

        this.chessboard.promote(input);
    }
}

module.exports = Engine;
