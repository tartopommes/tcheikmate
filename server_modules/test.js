
engine = new Engine()

//____________________________________________________________________________________________________________________________________________
function debugDisplay(engine) {
    //console.log(engine.chessboard.cases)
    console.log(engine.chessboard.displayBoard())
    console.log(engine.getFen());
}

//____________________________________________________________________________________________________________________________________________
function displayMovesList() {
    let movesList = engine.getMovesList()
    let display = `--== move list: ==--`
    for (let move of movesList) {
        display += `\n${move[0]} => `
        for (let newPos of move[1])
            display += `${newPos}, `
    }
    console.log(display)
    console.log(movesList)
}

//____________________________________________________________________________________________________________________________________________
function round(oldPos, newPos = undefined) {
    if (!engine.isPromotionAvailable()) {
        console.log(`engine.setUserMove(${oldPos}, ${newPos})`)
        engine.setUserMove(oldPos, newPos)

        debugDisplay(engine)
        console.log(`It is to --==[ ${engine.chessboard.actualPlayer} ]==-- player to play`)
        displayMovesList()
        console.log(`\n\n\n\n`)
        switch(engine.isGameOver()) {
            case false:
                console.log(`The game is not over.`)
                break
            case undefined:
                console.log(`${engine.chessboard.opponentColor(engine.chessboard.actualPlayer)} is in stalemate`)
                break
            default:
                console.log(`${engine.chessboard.isGameOver()} player win the game !`)
                break
        }
        if (engine.isPromotionAvailable())
            console.log(`A promotion is available, call round(your choice) between { '', 'q', 'r', 'n', 'b' }`)
    }
    else {
        if (newPos != undefined) {
            console.log(`A promotion is available, call round(your choice) between { '', 'q', 'r', 'n', 'b' }`)
            return
        }
        else
            engine.chessboard.promote(oldPos)
    }
}

//____________________________________________________________________________________________________________________________________________
function history() {
    for (let move of engine.chessboard.moveHistory) {
        let oldPos = move[0]
        let newPos = move[1]
        let pawnMoved = move[2]
        let name = pawnMoved.name
        let color = pawnMoved.color

        console.log(`${color} ${name} move from ${oldPos} to ${newPos}`)
    }
}



// Initialisation, usefull for debug only
console.log(engine.getFen())

let cases = ""
for (let i in engine.chessboard.cases) {
    if (i % 8 == 0) cases += `\n`
    cases += `${i} `
}
console.log(`${cases}\n\n\n\n`)

debugDisplay(engine)
console.log(engine.getMovesList())




// Play the game

round(53, 45)
round(11, 27)
round(57, 42)
history()
round(2, 29)
round(52,36)
round(3,19)
round(42,27)
round(19,46)
history()
