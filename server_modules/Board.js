// Utiliste la classe Piece, A DEPLACER
const Piece   = require('./Piece');
class Board {

    //____________________________________________________________________________________________________________________________________________
    constructor() {
        // Cas où les PAWNs black sont en haut
        /*this.draw = [ // if round(21,13)
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(`KING`,`black`),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(`ROOK`,`white`),   new Piece(`ROOK`,`white`),   new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(`KING`,`white`), new Piece(),                 new Piece(),                 new Piece()
        ];
        this.whitewin = [ // if round(22,13)
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(`KING`,`black`),   new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(`KING`,`white`), new Piece(),                 new Piece(`QUEEN`,`white`),  new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece()
        ];*/
        this.cases = [
            new Piece(`ROOK`,`black`), new Piece(`KNIGHT`,`black`), new Piece(`BISHOP`,`black`), new Piece(`QUEEN`,`black`), new Piece(`KING`,`black`), new Piece(`BISHOP`,`black`), new Piece(`KNIGHT`,`black`), new Piece(`ROOK`,`black`),
            new Piece(`PAWN`,`black`), new Piece(`PAWN`,  `black`), new Piece(`PAWN`,  `black`), new Piece(`PAWN`, `black`), new Piece(`PAWN`,`black`), new Piece(`PAWN`,  `black`), new Piece(`PAWN`,  `black`), new Piece(`PAWN`,`black`),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(),               new Piece(),                 new Piece(),                 new Piece(),                new Piece(),               new Piece(),                 new Piece(),                 new Piece(),
            new Piece(`PAWN`,`white`), new Piece(`PAWN`,  `white`), new Piece(`PAWN`,  `white`), new Piece(`PAWN`, `white`), new Piece(`PAWN`,`white`), new Piece(`PAWN`,  `white`), new Piece(`PAWN`,  `white`), new Piece(`PAWN`,`white`),
            new Piece(`ROOK`,`white`), new Piece(`KNIGHT`,`white`), new Piece(`BISHOP`,`white`), new Piece(`QUEEN`,`white`), new Piece(`KING`,`white`), new Piece(`BISHOP`,`white`), new Piece(`KNIGHT`,`white`), new Piece(`ROOK`,`white`)
        ];

        this.actualPlayer = `white`;
        this.enPassant = -1; // Number id of the square where to take 'en pasant'. Used in pawnMoves() in class Piece
        this.canBePromoted = false; // If a pawn can be promoted to QUEEN, KNIGHT, BISHOP or ROOK.

        this.moveHistory = new Array();
        this.halfMoveNumber = 0;

        // Castlings
        this.kingsideWhiteCastling = true; // White can castle kingside
        this.queensideWhiteCastling = true; // White can castle queenside
        this.kingsideBlackCastling = true;
        this.queensideBlackCastling = true;
    }

    //____________________________________________________________________________________________________________________________________________
    // Returns all possible moves for the requested color.
    getMoveList(color = '', isAttacked = false) {
        if (color == '')
            color = this.actualPlayer;

        let movesArray = new Array();

        for (let it = 0; it < this.cases.length; it++) {
            let piece = this.cases[it];

            // Piece (or empty square) color is not the wanted ? pass
            if (piece.color == color) {
                let opponentColor = this.opponentColor(color);
                switch (piece.name) {
                    case `KING`:
                        movesArray = movesArray.concat(piece.kingMoves(it, opponentColor, this, isAttacked));
                        break;
                    case `QUEEN`:
                        movesArray = movesArray.concat(piece.queenMoves(it, opponentColor, this));
                        //movesArray = movesArray.concat(piece.rookMoves(it, opponentColor, this));
                        //movesArray = movesArray.concat(piece.bishopMoves(it, opponentColor, this));
                        break;
                    case `ROOK`:
                        movesArray = movesArray.concat(piece.rookMoves(it, opponentColor, this));
                        break;
                    case `KNIGHT`:
                        movesArray = movesArray.concat(piece.knightMoves(it, opponentColor, this));
                        break;
                    case `BISHOP`:
                        movesArray = movesArray.concat(piece.bishopMoves(it, opponentColor, this));
                        break;
                    case `PAWN`:
                        movesArray = movesArray.concat(piece.pawnMoves(it, color, this));
                        break;
                }
            }
            //debug
            else if (piece == undefined) {
                console.log(`piece ${it} is undefined`)
                console.log(this.cases[it])
            }
        }
        return movesArray;
    }

    //____________________________________________________________________________________________________________________________________________
    displayBoard() {
        /**
         * Used to debug the game
         */
        let display = "";
        let currentName = '';
        let i = 0;

        display += `    `;
        for (let it = 0; it < 8; it++)
            display += String.fromCharCode(97 + it) + " ";
        display += `\n`;

        for (let piece of this.cases) {
                currentName = piece.name;
                if ( i % 8 == 0)
                    display += `\n ${(i/8 + 1)}  `;

                switch (currentName) {
                    case `KING`:
                        display += ((piece.color == 'white') ? 'K' : 'k');
                        break;
                    case `QUEEN`:
                        display += ((piece.color == 'white') ? 'Q' : 'q');
                        break;
                    case `ROOK`:
                        display += ((piece.color == 'white') ? 'R' : 'r');
                        break;
                    case `KNIGHT`:
                        display += ((piece.color == 'white') ? 'N' : 'n');
                        break;
                    case `BISHOP`:
                        display += ((piece.color == 'white') ? 'B' : 'b');
                        break;
                    case `PAWN`:
                        display += ((piece.color == 'white') ? 'P' : 'p');
                        break;
                    case `.`:
                        display += `.`;
                        break;
                }
                display += ' ';
                i += 1;
        }
        return display;
    }

    //____________________________________________________________________________________________________________________________________________
    getFENBoard(onlyTheDisplay) {
        /**
         * Return the FEN notation of the current Board.
         * "OnlyTheDisplay" tell us if we want only the first FEN case(== the board) [true],
         * or everything (including the player, castling, history..) [false].
         */

        let emptySquare = 0; // Nb of empty square.

        let fenString = '';

        for (let i in this.cases) {
            let piece = this.cases[i];
            let name = piece.name;
            let color = piece.color;

            if (emptySquare == 8) {
                fenString += '8';
                emptySquare = 0;
            }

            if (i % 8 == 0) { // The end of a row. /xxxxxxxx/
                if (emptySquare > 0) {
                    fenString += emptySquare.toString();
                    emptySquare = 0;
                }
                fenString += '/'; // We can move to the next row by adding a "/".
            }

            if (piece.isEmpty())
                emptySquare += 1;
            else {
                if (emptySquare > 0) {
                    fenString += emptySquare.toString();
                    emptySquare = 0;
                }
                switch (name) {
                    case `KING`:
                        fenString += (color == 'white') ? 'K' : 'k';
                        break;
                    case `QUEEN`:
                        fenString += (color == 'white') ? 'Q' : 'q';
                        break;
                    case `ROOK`:
                        fenString += (color == 'white') ? 'R' : 'r';
                        break;
                    case `KNIGHT`:
                        fenString += (color == 'white') ? 'N' : 'n';
                        break;
                    case `BISHOP`:
                        fenString += (color == 'white') ? 'B' : 'b';
                        break;
                    case `PAWN`:
                        fenString += (color == 'white') ? 'P' : 'p';
                        break;
                }
            }
        }


        //fin de l'ajout des caractères du tableau, on ajoute
        //le coté à qui c'est le tour etc
        if (emptySquare > 0)
            fenString += emptySquare.toString();


        // If we juste want the board diplay
        if (onlyTheDisplay)
            return fenString;

        fenString += (this.actualPlayer == 'white') ? ' w ' : ' b ';

        let noCastleRight = true;

        if (this.kingsideWhiteCastling == true) {
            fenString += 'K';
            noCastleRight = false;
        }
        if (this.queensideWhiteCastling == true) {
            fenString += 'Q';
            noCastleRight = false;
        }
        if (this.kingsideBlackCastling == true) {
            fenString += 'k';
            noCastleRight = false;
        }
        if (this.queensideBlackCastling == true) {
            fenString += 'q';
            noCastleRight = false;
        }

        fenString += (this.enPassant != -1) ? ' ' + String.fromCharCode(97 + this.getCol(this.enPassant)) + (8 - this.getRow(this.enPassant)) : ' -'; // Convert from int (0 to 63) to char ('a8' to 'h1')

        fenString += ' -';

        fenString += ' ' + this.moveHistory.length.toString();


        return fenString;
    }

    //____________________________________________________________________________________________________________________________________________
    makeMove(actualPos, newPos) {

        let movedPiece = this.cases[actualPos]; // Original piece moved (does not take into account promotions).
        let caughtPiece = this.cases[newPos]; // Piece taken, the one that will be replaced by the movedPiece.
        let isEnPassant = false; // Is the move is an "En Passant" move?
        let name = movedPiece.name;
        let color = movedPiece.color;

        let enPassantSave = this.enPassant; // Save actual enPassant value (used for moveHistory).
        let kWCSave = this.kingsideWhiteCastling;
        let qWCSave = this.queensideWhiteCastling;
        let kBCSave = this.kingsideBlackCastling;
        let qBCSave = this.queensideBlackCastling;

        let eraseEnPassant = true; // Used to know if it's EnPassant or not. The "En passant" capture can only be made after the opponent has moved two squares.

        // Make move
        this.cases[newPos] = this.cases[actualPos];
        this.cases[actualPos] = new Piece();

        this.halfMoveNumber += 1; // One more move made.

        // Manages the type of part being moved.
        switch(name) {
            // PAWN ========================================================== //
            case `PAWN`:
                //debug
                console.log("make pawn move")
                // Color-dependent variables.
                let nextRow = (color == `white`) ? 8 : -8; // Changes the sign of the move according to the player who is playing.
                let is2SquaresMoves = (color == `white`) ? this.getRow(actualPos) == 6 && this.getRow(newPos) == 4 : this.getRow(actualPos) == 1 && this.getRow(newPos) == 3;
                let isItTheLastRow = (color == `white`) ? (a) => a < 8 : (a) => a > 55;
                //debug
                //console.log(`actualPos: ${actualPos}, newPos: ${newPos}, nextRow: ${nextRow}, is2SquaresMoves: ${is2SquaresMoves}, isItTheLastRow(newPos): ${isItTheLastRow(newPos)}, this.enPassant: ${this.enPassant}`)

                // If a white pawn moves 2 squares forward, from the seventh (7) to the fifth (5) row (out of 8).
                // A Black pawn, also on the fifth (5) row and on an adjacent column, may make an "En Passant" move to the next turn.
                if (is2SquaresMoves) {
                    this.enPassant = newPos + nextRow;
                    eraseEnPassant = false;
                }
                else if (this.enPassant == newPos) { // It is an en passant move?
                    caughtPiece = this.cases[newPos + nextRow];
                    this.cases[newPos + nextRow] = new Piece();
                    isEnPassant = true;
                }
                else if (isItTheLastRow(newPos)) // If the pawn arrived on the last row, it can be promoted to QUEEN, KNIGHT, BISHOP or ROOK.
                    this.canBePromoted = true;
                break;

            // ROOK ========================================================== //
            case `ROOK`:
                //debug
                console.log("make rook move")
                // Update castle rights
                let kingsideRook = (color == `white`) ? 56 : 0;
                let queensideRook = (color == `white`) ? 63 : 7;

                if (actualPos == kingsideRook)
                    this.kingsideWhiteCastling = false;
                else if (actualPos == queensideRook)
                    this.queensideWhiteCastling = false;
                break;

             // KING ========================================================== //
            case `KING`:
                //debug
                console.log("make king move")
                let startingPosition = (color == `white`) ? 60 : 4;

                if (actualPos == startingPosition) {

                    let kingsideCastling = (color == `white`) ? 58 : 2;
                    let quennsideCastling = (color == `white`) ? 62 : 6;

                    // Update castle rights
                    (color == `white`) ? this.kingsideWhiteCastling = false : this.kingsideBlackCastling = false; // Kingside
                    (color == `white`) ? this.queensideWhiteCastling = false : this.queensideBlackCastling = false; // Queenside

                    // If castling => move the ROOK
                    if (newPos == kingsideCastling || newPos == quennsideCastling) {

                        let actualRookPos = (newPos == kingsideCastling) ? ((color == `white`) ? 56 : 0) : ((color == `white`) ? 63 : 7);
                        let newRookPos = (newPos == kingsideCastling) ? ((color == `white`) ? 59 : 3) : ((color == `white`) ? 61 : 5);

                        this.cases[actualRookPos] = new Piece();
                        this.cases[newRookPos] = new Piece(`ROOK`, `white`);
                    }
                }
                break;
        }

        // If there is no longer any possibility of doing an "En Passant" move.
        if (eraseEnPassant)
            this.enPassant = -1;

        this.switchingPlayer();

        this.moveHistory.push([
            actualPos, newPos, movedPiece, caughtPiece, isEnPassant, enPassantSave,
            /*promoteTo,*/ kWCSave, qWCSave, kBCSave, qBCSave
        ]);

        //==/!\==/!\==/!\==/!\==//  MODIFIER POUR S'ADAPTER AU NOUVEAU MODE DE TRANSIT INTESTINAL //==/!\==/!\==/!\==/!\==//
        if (this.isInCheck(color)) {
            this.undoMove();
            return false;
        }
        return true;
    }

    //____________________________________________________________________________________________________________________________________________
    undoMove() {

        let lastMove = this.moveHistory[this.moveHistory.length - 1];
        // Retrieves the data from the last move
        let oldPos =                  lastMove[0];
        let newPos =                  lastMove[1];
        let movedPiece =              lastMove[2]; // Original piece moved (does not take into account promotions).
        let caughtPiece =             lastMove[3]; // Piece taken, the one that has been replaced by the movedPiece.
        let isEnPassant =             lastMove[4];
        let enPassantSave =           lastMove[5];
        /*let promoteTo =               lastMove[6];*/
        this.kingsideWhiteCastling =  lastMove[6];
        this.queensideWhiteCastling = lastMove[7];
        this.kingsideBlackCastling =  lastMove[8];
        this.queensideBlackCastling = lastMove[9];

        let name = movedPiece.name;
        let color = movedPiece.color;

        this.halfMoveNumber -= 1;
        this.switchingPlayer();

        this.enPassant = enPassantSave;

        // --==  Reset moved piece. ==--
        this.cases[oldPos] = movedPiece; // All move.

        // --== Reset caught piece. ==--
        if (name == `PAWN` && isEnPassant) { // "En Passant" move.
            let previousRow = (color == `white`) ? 8 : -8;
            this.cases[newPos + previousRow] = caughtPiece;
        }
        else // Classic move.
            this.cases[newPos] = caughtPiece;

        // Reset ROOK if it was a castle.
        if (name == `KING` && (oldPos == 60 || oldPos == 4)) { // --== If castle => Replace ROOK. ==--
            let kingsideCastling = (color == `white`) ? 58 : 2;
            let quennsideCastling = (color == `white`) ? 62 : 6;

            if (newPos == kingsideCastling || newPos == quennsideCastling) {
                let oldRookPos = (newPos == kingsideCastling) ? ((color == `white`) ? 56 : 0) : ((color == `white`) ? 63 : 7);
                let newRookPos = (newPos == kingsideCastling) ? ((color == `white`) ? 59 : 3) : ((color == `white`) ? 61 : 5);

                this.cases[oldRookPos] = new Piece(`ROOK`, color);
                this.cases[newRookPos] = new Piece();
            }
        }

        if (this.canBePromoted) // Undo askForPromotion
            this.canBePromoted = false;

        // Pop last move from history
        this.moveHistory.pop();
    }

    // initAiVariables() {
    //     this.maxHalfMove = 32;
    //     this.smthLenght = Array.from({length: this.maxPly}, (_, n) =>  0);
    //     this.inf = 32000;
    //     this.initDepth = 4;
    // }

    // //____________________________________________________________________________________________________________________________________________
    // aiSearching() {
    //     /**
    //      *
    //      */

    //     this.bestMoves = Array.from({length: this.maxPly}, (_, n) =>  Array.from({length: this.maxPly}, (_, n) => 0)) // Array of best moves
    //     this.nodes = 0; // nb of nodes
    //     this.halfMoveNumber = 0; //nb of move for both player together (+1 for each different color move)

    //     // do search lopping in function of depth number
    //     for (let depth in this.initDepth) {
    //         let score = this.isProto(depth + 1, -this.inf, this.inf); // Returns a score of a search based on a given depth.

    //         // debug / print
    //         let j = 0;
    //         while (this.bestMoves[j][j] != 0) {
    //             let [oldPos, newPos] = this.bestMoves[j][j];
    //             console.log(`${oldPos},${newPos},${sth}, `);
    //             j++;
    //         }

    //         if (score > this.inf - 100 || score < -this.inf + 100) // if : score isn't in { -31900 ; 31900 } : a mat is found. BUT WHY ?
    //             break;
    //     }

    //     let best = this.bestMoves[0][0]; // the best move will be at [0][0] of bestMoves[][]
    //     this.makeMove(best[0], best[1]) // do it

    //     //debug / check if the game is over
    //     switch(this.isGameOver()) {
    //         case false:
    //             console.log(`The game is not over.`);
    //             break;
    //         case undefined:
    //             console.log(`${engine.chessboard.opponentColor(engine.chessboard.actualPlayer)} is in stalemate`);
    //             break;
    //         default:
    //             console.log(`${engine.chessboard.isGameOver()} player win the game !`);
    //             break;
    //     }
    // }

    // //____________________________________________________________________________________________________________________________________________
    // aiProto(depth, a, b) {
    //     /**
    //      *
    //      */

    //     if (depth == 0)
    //         return this.scoreEvaluation;

    //     this.nodes++;
    //     this.smthLenght[this.halfMoveNumber] = halfMoveNumber;

    //     // Do not go too deep
    //     if (this.halfMoveNumber >= this.maxHalfMove - 1)
    //         return this.scoreEvaluation;

    //     // If king is in check, let's go deeper
    //     let isKingInCheck = this.isInCheck(this.actualPlayer)
    //     if (isKingInCheck)
    //         depth++;

    //     let movesList = this.getMoveList();
    //     let atLeastOneMove = false;

    //     for (let move of movesList) {

    //         let oldPos = move[0];

    //         for (let newPos of move[1]) {

    //             /* If 'move' lets king in check, undo it and ignore it. */
    //             if (!this.makeMove(oldPos, newPos))
    //                 continue;

    //             atLeastOneMove = true;
    //             let score = -this.iaProto(depth - 1, -b, -a);
    //             this.undoMove();

    //             if (score > a) {
    //                 if (score >= b)
    //                     return b;
    //                 a = score;

    //                 // updating bestMoves
    //                 this.bestMoves[this.halfMoveNumber][this.halfMoveNumber] = [oldPos, newPos]
    //                 let j = this.halfMoveNumber + 1;
    //                 while (j < this.smthLenght[this.halfMoveNumber + 1]) {
    //                     this.bestMoves[this.halfMoveNumber][j] = this.bestMoves[this.halfMoveNumber + 1][j];
    //                     this.smthLenght[this.halfMoveNumber] = this.bestMoves[this.halfMoveNumber + 1];
    //                 }
    //             }
    //         }

    //         // If no move hase been done : it is draw or mat
    //         if (!atLeastOneMove) {
    //             if (isKingInCheck)
    //                 return -this.inf + this.halfMoveNumber;
    //             else
    //                 return 0;
    //         }
    //         return a;
    //     }
    // }

    // //____________________________________________________________________________________________________________________________________________
    // scoreEvaluation() {
    //     /**
    //      * Return actual 'material score' for the actualPlayer
    //      */
    //     let whiteScore = 0;
    //     let blackScore = 0;

    //     for (let piece of this.cases) {
    //         if (piece.color == `white`)
    //             whiteScore += piece.value;
    //         else
    //             blackScore += piece.value;
    //     }

    //     return (this.actualPlayer == `white`) ? whiteScore - blackScore : blackScore - whiteScore;
    // }

    //____________________________________________________________________________________________________________________________________________
    promote(promoteTo) {
        /**
         *
         */
        let lastMove = this.moveHistory[len(this.moveHistory) - 1];
        let pawn = lastMove[1];
        let color = this.cases[pawn].color;

        // PROMOTE TO
        switch(promoteTo) {
            case `q`:
                this.cases[pawn] = new Piece(`QUEEN`, color);
                break;
            case `r`:
                this.cases[pawn] = new Piece(`ROOK`, color);
                break;
            case `n`:
                this.cases[pawn] = new Piece(`KNIGHT`, color);
                break;
            case `b`:
                this.cases[pawn] = new Piece(`BISHOP`, color);
                break;
            default:
                break;
        }

        this.canBePromoted = false;
    }

    //____________________________________________________________________________________________________________________________________________
    switchingPlayer() {
        /** */
        this.actualPlayer = (this.actualPlayer == `white`) ? `black` : `white`;
    }

    //____________________________________________________________________________________________________________________________________________
    opponentColor(color) {
        /** */
        if (color == `white`)
            return `black`;
        return `white`;
    }

    //____________________________________________________________________________________________________________________________________________
    isInCheck(color) {
        /* Return a boolean to show if the king is in check.
           First, we find our king, and then we check if he is attacked. */

        for (let pos = 0; pos < 64; pos++)
            if (this.cases[pos].name == 'KING' && this.cases[pos].color == color)
                return (this.isAttacked(pos, this.opponentColor(color)));

        throw "No king found ! from Board.js in isInCheck() method"; // If no king found
    }

    //____________________________________________________________________________________________________________________________________________
    isAttacked(pos, color) {
        /* Return a boolean to say if the position 'pos' will be attacked
           by the color 'color'.
           To do so, we generate all the moves for the color color and check
           if 'pos' is in the tab. */

        let attackedMoveList = this.getMoveList(color, true);

        let found = attackedMoveList.find(
            moveList => moveList[1].find(
                move => move == pos) == pos); // As a reminder: moves [1] are the possible destinations of a piece.

        return (found == undefined) ? false : true;
    }

    //____________________________________________________________________________________________________________________________________________
    getRow(pos) {
        // As it's a 8 per 8 board, we can use binary functions :
        // Shifts the position in binary representation by 3 bits to the right, rejecting the bits to the right.
        return pos >> 3;
    }

    //____________________________________________________________________________________________________________________________________________
    getCol(pos) {
        // As it's a 8 per 8 board, we can use binary functions :
        // Returns a 1 for each bit position for which the bits corresponding to 'pos' and '7' are 1.
        return pos & 7;
    }

    //____________________________________________________________________________________________________________________________________________
    isGameOver() {
        /**
         * If players played more than 50 moves, return true
         * If a player win, return his color
         * Else, it's a nul match, return undefined (it's a stalemate)
         * Else, is the game isn't over, return false
         */
        if (this.halfMoveNumber > 50)
            return true;

        let movesList = this.getMoveList();
        for (let move of movesList) {
            let oldPos = move[0];
            for (let newPos of move[1])
                // Si un déplacement est possible, c'est que le roi n'est pas en échec et/ou que le joueur peut encore faire un move legal,
                // le jeu n'est donc pas over
                if (this.makeMove(oldPos, newPos))  {
                    this.undoMove();
                    return false;
                }
        }
        // Si il n'y a plus de déplacement possible pour le joueur et que son king est en echec, il est donc en echec et mat et il a perdu
        // Sinon, c'est qu'il y a match nul
        console.log(this.actualPlayer);
        return (this.isInCheck(this.actualPlayer)) ? this.opponentColor(this.actualPlayer) : undefined;
    }
}

module.exports = Board;
