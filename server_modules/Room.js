
//Cette classe va venir référencer toutes les GameRoomServerSide
//utilisée et référencée
class Room{
   constructor(){
      this.listOfRooms = []
   }
   //Cette fonction ajoute un objet GameRoomServerSide à la liste
   addNewRoom(room){
      this.listOfRooms.push(room);
   }

   //Cette fonction enleve un joueur de la liste de ceux qui attendent en queue
   removeRoom(room){
      this.listOfRooms = this.listOfRooms.filter(obj => obj.id =! room.id); //On vient filtrer dans
      //la liste tout ceux qui ont id correspond à celui du joueur se retirant
   }

   getLastestRoom(){
      return(this.listOfRooms[this.listOfRooms.length - 1])
   }

   getRoomWithSocketID(socketID){
      //On filtre la liste de toute les rooms pour trouver dans quelle room
      //on peut trouver le socketID (soit c'est l'id de joueur1, soit de joueur2,
      // soit c'est l'id de quelqu'un dans une autre salle)
      let roomToReturn = undefined;
      for(let room of this.listOfRooms ){
         if (room.player1 != undefined && room.player2 != undefined){

            if (room.player1.socket.id == socketID || room.player2.socket.id == socketID){
               roomToReturn = room
            }
         }
      }
      return roomToReturn
   }
   showDEBUG(){
      return(this.listOfRooms[this.listOfRooms.length - 1].game)
   }

}

module.exports= Room;
