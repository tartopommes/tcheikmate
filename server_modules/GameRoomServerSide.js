//

class GameRoomServerSide{
  constructor() {
    this.id = GameRoomServerSide.incrementId()
    this.player1 = undefined;
    this.player2 = undefined;
    this.isRunning = false;
    this.game = undefined;
  }

  static incrementId() {
    if (!this.latestId) {
      this.latestId = 1;
   }
    else {
      this.latestId++
   }
    return this.latestId
  }

  setPlayer1(player){
     this.player1 = player
  }
  setPlayer2(player){
     this.player2 = player
 }
}

module.exports = GameRoomServerSide;
