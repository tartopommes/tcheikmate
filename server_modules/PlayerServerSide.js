//Cette classe représente un joueur,
// un joueur est doté d'un nom, d'un skillRating et d'un socket attribué
//Il a aussi 2 booléen pour savoir si il est en recherche ou en jeu

class PlayerServerSide{
   constructor(name,sr,socket){
      this.name = name;
      this.skillRating = sr;
      this.socket = socket;
      this.isSearching = false;
      this.isInGame = false;
   }

   setNewSocket(newSocket){
      this.socket = newSocket
   }
}

module.exports = PlayerServerSide;
