class Piece {
    // Name of the pieces
    pieceNames = [`.`,`KING`,`QUEEN`,`ROOK`,`KNIGHT`,`BISHOP`,`PAWN`];

    // Give a score value for each piece : KING=0, QUEEN=9, ROOK=5...
    pieceValues = [0, 0, 9, 5, 3, 3, 1];

    // For the pieces moves, using method "mail box" from Robert Hyatt
    // It helps to know if a piece is not moved outside the board !
    tab120 = [
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1,  0,  1,  2,  3,  4,  5,  6,  7, -1,
        -1,  8,  9, 10, 11, 12, 13, 14, 15, -1,
        -1, 16, 17, 18, 19, 20, 21, 22, 23, -1,
        -1, 24, 25, 26, 27, 28, 29, 30, 31, -1,
        -1, 32, 33, 34, 35, 36, 37, 38, 39, -1,
        -1, 40, 41, 42, 43, 44, 45, 46, 47, -1,
        -1, 48, 49, 50, 51, 52, 53, 54, 55, -1,
        -1, 56, 57, 58, 59, 60, 61, 62, 63, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    ];
    tab64 = [
        21, 22, 23, 24, 25, 26, 27, 28,
        31, 32, 33, 34, 35, 36, 37, 38,
        41, 42, 43, 44, 45, 46, 47, 48,
        51, 52, 53, 54, 55, 56, 57, 58,
        61, 62, 63, 64, 65, 66, 67, 68,
        71, 72, 73, 74, 75, 76, 77, 78,
        81, 82, 83, 84, 85, 86, 87, 88,
        91, 92, 93, 94, 95, 96, 97, 98
    ];

    // Moving vectors according to the `tab64`,
    rookPositions = [-10, 10, -1, 1];
    bishopPositions = [-11, -9, 11, 9];
    knightPositions = [-12, -21, -19, -8, 12, 21, 19, 8];
    // No need to creat King and Queen beacause they are equal to rook + bishop positions)

    //____________________________________________________________________________________________________________________________________________
    //CONSTRUCTOR
    constructor(name = `.`, color = `.`) {
        this.name = name;
        this.color = color;
        this.value = this.pieceValues[this.pieceNames.indexOf(name)];
    }

    //____________________________________________________________________________________________________________________________________________
    isEmpty() {
        /* Return if the case is empty */
        return this.name == `.`;
    }


    //=================================================================================================================//
    // Les functions suivantes renvoie un tableau avec les déplacments possibles pour chaque pions.                    //
    // Les déplacements sont sous la forme : [déplacement d'origine, déplacement de destination, 'changement de pion'].//
    // Le changement de pion concerne les 'pawn' qui sont arrivé à l'opposé de l'échequier.                            //
    //=================================================================================================================//

    //____________________________________________________________________________________________________________________________________________
    kingMoves(pos, opponentColor, chessboard, isAttacked = false) {
        /* Return KING moves */

        let destinationList = new Array(); // Array of possible destinations for 'pos'
        const rookAndBishopPositions = this.rookPositions.concat(this.bishopPositions);

        //==== "Attack moves" ====//

        for (let move of rookAndBishopPositions) {
            let newPos = this.tab120[this.tab64[pos] + move];
            // Check if we are not out of the board
            if (newPos != -1)
                if (chessboard.cases[newPos].isEmpty() || chessboard.cases[newPos].color == opponentColor)
                    destinationList.push(newPos);
        }

        // Just return attack moves
        if (isAttacked)
            return (destinationList.length != 0) ? [[ pos, destinationList ]] : Array(0); //movesList

        // Get the color of the "actualPlayer"
        //oponnent color* ?
        const color = (opponentColor == `white`) ? `black` : `white`;

        //==== "Castling moves" ====//

        if (color == `white`) {
            // Kingside White Castle
            if (chessboard.kingsideWhiteCastling)
                // If a rook is at square 63 [AND] if squares between KING and ROOK are empty [AND] if squares on which KING walks are not attacked [AND] if KING is not in check
                // Then we can add this castle move
                if (chessboard.cases[63].name == `ROOK` && chessboard.cases[63].color == `white`)
                    if (chessboard.cases[61].isEmpty() && chessboard.cases[62].isEmpty())
                        if (!chessboard.isAttacked(61, opponentColor) && !chessboard.isAttacked(62, opponentColor) &&!chessboard.isAttacked(pos, opponentColor))
                            destinationList.push(62);
            // Quennside White Castle
            if (chessboard.queensideWhiteCastling)
                if (chessboard.cases[56].name == `ROOK` && chessboard.cases[56].color == `white`)
                    if (chessboard.cases[57].isEmpty() && chessboard.cases[58].isEmpty() && chessboard.cases[59].isEmpty())
                        if (!chessboard.isAttacked(58, opponentColor) && !chessboard.isAttacked(59, opponentColor) &&!chessboard.isAttacked(pos, opponentColor))
                            destinationList.push(58);
        }
        else if (color == `black`) {
            // Kingside Black Castle
            if (chessboard.kingsideBlackCastling)
                if (chessboard.cases[7].name == `ROOK` && chessboard.cases[7].color == `white`)
                    if (chessboard.cases[5].isEmpty() && chessboard.cases[6].isEmpty())
                        if (!chessboard.isAttacked(5, opponentColor) && !chessboard.isAttacked(6  , opponentColor) &&!chessboard.isAttacked(pos, opponentColor))
                            destinationList.append(6);
            // Quennside Black Castle
            if (chessboard.queensideBlackCastling)
                if (chessboard.cases[0].name == `ROOK` && chessboard.cases[0].color == `white`)
                    if (chessboard.cases[1].isEmpty() && chessboard.cases[2].isEmpty() && chessboard.cases[3].isEmpty())
                        if (!chessboard.isAttacked(2, opponentColor) && !chessboard.isAttacked(3, opponentColor) &&!chessboard.isAttacked(pos, opponentColor))
                            destinationList.append(2);
        }

        return (destinationList.length != 0) ? [[ pos, destinationList ]] : Array(0); // movesList
    }

    //____________________________________________________________________________________________________________________________________________
    knightMoves(pos, opponentColor, chessboard) {
        /* Return KNIGHT moves */
        let destinationList = new Array();
        for (let move of this.knightPositions) {
            let newPos = this.tab120[this.tab64[pos] + move];
            if (newPos != -1) // Not out of board
                if (chessboard.cases[newPos].isEmpty() || chessboard.cases[newPos].color == opponentColor) // newPos is empty or their is an oppenant
                    destinationList.push(newPos); // Add the move
        }

        return (destinationList.length != 0) ? [[ pos, destinationList ]] : Array(0); // movesList
    }

    //____________________________________________________________________________________________________________________________________________
    rookMoves(pos, opponentColor, chessboard) {
        /* Return ROOK moves */

        let destinationList = new Array();
        for (let move of this.rookPositions) {
            let newPos = -1;
            let it = 1;
            do {
                newPos = this.tab120[this.tab64[pos] + move * it];
                if (newPos != -1) { // Not out of board
                    if (chessboard.cases[newPos].isEmpty() || chessboard.cases[newPos].color == opponentColor) // newPos is empty or their is an oppenant
                        destinationList.push(newPos); // Add the move
                }
                it++;
            } while (newPos != -1 && chessboard.cases[newPos].isEmpty()); // Stop if outside of board or newPos is not empty (== their is already a piece)
        }

        return (destinationList.length != 0) ? [[ pos, destinationList ]] : Array(0); // movesList
    }

    //____________________________________________________________________________________________________________________________________________
    bishopMoves(pos, opponentColor, chessboard) {
        /* Return BISHOP moves */

        let destinationList = new Array();
        for (let move of this.bishopPositions) {
            let newPos = -1;
            let it = 1;
            do {
                newPos = this.tab120[this.tab64[pos] + move * it];
                if (newPos != -1) // Not out of board
                    if (chessboard.cases[newPos].isEmpty() || chessboard.cases[newPos].color == opponentColor) // newPos is empty or their is an oppenant
                        destinationList.push(newPos) // Add the move
                it++;
            } while (newPos != -1 && chessboard.cases[newPos].isEmpty()); // Stop if outside of board or newPos is not empty (== their is already a piece)
        }

        return (destinationList.length != 0) ? [[ pos, destinationList ]] : Array(0); // movesList
    }

    //____________________________________________________________________________________________________________________________________________
    queenMoves(pos, opponentColor, chessboard) {
        /* Return QUEEN moves */

        let rookDestination = this.rookMoves(pos, opponentColor, chessboard);
        let bishopDestination = this.bishopMoves(pos, opponentColor, chessboard);

        if (rookDestination.length != 0 && bishopDestination.length != 0)
            return [[ pos, rookDestination[0][1].concat(bishopDestination[0][1]) ]];

        else if (rookDestination.length != 0)
            return [[ pos, rookDestination[0][1] ]];

        else if (bishopDestination.length != 0)
            return [[ pos, bishopDestination[0][1] ]];

        return Array(0);
    }

    //____________________________________________________________________________________________________________________________________________
    pawnMoves(pos, color, chessboard) {
        /* Return PAWN moves */

        let destinationList = new Array();

        // Color-dependent variables.
        let sideSign = (color == 'white') ? (a) => a * -1 : (a) => a; // Changes the sign of the move according to the player who is playing.
        let maxRow = (color == 'white') ? (a) => a < 8 : (a) => a > 55; // Move this let to Engine() class
        let startingRow = (color == 'white') ? 6 : 1; // Changes the starting row according to the player who is playing.


        //===== CLASSIC MOVES =====//

        // Move up to next square
        let newPos = this.tab120[this.tab64[pos] + sideSign(10)];
        if (newPos != -1) {
            // If the pawn has reached the last row (i.e. the squares from 0 to 7 for 'white' or 56 to 63 for 'black')
            // It will be promoted by sending a request to client from engine (make choice later), so it's not here.
            if (chessboard.cases[newPos].isEmpty())
                destinationList.push(newPos);
        }


        //===== STARTING MOVES =====//

        // Move to two upper square if pawn is at starting square (no need to check if we are out of the board)
        if (chessboard.getRow(pos) == startingRow)
            if (chessboard.cases[pos + sideSign(8)].isEmpty() && chessboard.cases[pos + sideSign(16)].isEmpty())
                destinationList.push(pos + sideSign(16));


        //===== ATTACK MOVES =====//
        const opponentColor = (color == `white`) ? `black` : `white`;

        // Move and capture UPPER Left square for 'white' or BOTTOM Left square for 'black'
        newPos = this.tab120[this.tab64[pos] + sideSign(11)];
        if (newPos != -1) {
            if (chessboard.cases[newPos].color == opponentColor || chessboard.enPassant == newPos) // Check if the destination square is an oppenant or an 'enPassant' possibility
                destinationList.push(newPos);
        }
        // Move and capture UPPER Right square for 'white' or BOTTOM Right square for 'black'
        newPos = this.tab120[this.tab64[pos] + sideSign(9)];
        if (newPos != -1) {
            if (chessboard.cases[newPos].color == opponentColor || chessboard.enPassant == newPos)
                destinationList.push(newPos);
        }
        return (destinationList.length != 0) ? [[ pos, destinationList ]] : Array(0); // movesList
    }
}

module.exports = Piece;
