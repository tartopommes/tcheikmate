const fs = require('fs');
const Score = require('./Score')
module.exports={

   exportDataFromFile(path,socket){
   // Read users.json file
      fs.readFile(path, function(err, data) {

          // Check for errors
          if (err) throw err;

          // Converting to JSON
          const scoreboard = JSON.parse(data);
          socket.emit('retrieveScoreBoard',scoreboard)
      });

   },


   saveDataToFile(path,dataToAdd){
      fs.readFile(path, function(err, data) {

         // Check for errors
         if (err) throw err;

         // Converting to JSON
         const scoreboard = JSON.parse(data);
         scoreboard.push(dataToAdd)
         fs.writeFile(path,JSON.stringify(scoreboard),err =>{
            if(err) throw err;
         })
      });

   },

   //Scoring :
   // 1 pion = 5 points
   //
   calculateScore(player1,player2,colorWinner,fenBoard){
      let pts = 0;
      let name;
      let sr;
      if (colorWinner === "white"){
         name = player1.name;
         sr = player1.skillRating;
      }
      else{
         name = player2.name;
         sr = player2.skillRating;
      }

      for (let i=0; i<=fenBoard.length ;i++){
         let charFen;
         charFen = fenBoard.charAt(i);
         if(colorWinner === "white"){
            switch(charFen){
               case 'P':
                  pts += 5;
                  break;
               case 'R':
                  pts += 25;
                  break;
               case 'B':
                  pts += 20;
                  break;
               case 'N':
                  pts += 15;
                  break;
               case 'Q':
                  pts += 50;
                  break;
               default:
                  break;
            }
         }
         else if (colorWinner === "black"){
            switch(charFen){
               case 'p':
                  pts += 5;
                  break;
               case 'r':
                  pts += 25;
                  break;
               case 'b':
                  pts += 20;
                  break;
               case 'n':
                  pts += 15;
                  break;
               case 'q':
                  pts += 50;
                  break;
               default:
                  break;
            }
         }
      }
      let score = new Score(name,sr,pts);
      return score;
   }
}
