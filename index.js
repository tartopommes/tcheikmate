const port = 8100; //8080


//require vient de nodejs
const express = require('express');
const app = express(); // dans app on va venir créer le serveur et celui ci se base sur express (et simplifie la gestion d'un serveur)
const server = require('http').createServer(app);
const io = require('socket.io')(server);





const PlayerServerSide   = require('./server_modules/PlayerServerSide');
const GameRoomServerSide = require('./server_modules/GameRoomServerSide');
const Queue = require('./server_modules/Queue');
const Room  = require('./server_modules/Room');
const Engine  = require('./server_modules/Engine');

const moduleScore  = require('./server_modules/moduleScore');

const allPlayersInQueue = new Queue();
const allRooms = new Room();
const pathToScoreboard = "scoreboard.json";



app.use(express.static(__dirname + '/assets/'));



app.get('/',(req, res, next) => {

   res.sendFile(__dirname + '/assets/views/mainMenu.html');
});

app.get('/game',(req, res, next) => {

   res.sendFile(__dirname + '/assets/views/game.html')
});

//Pour toutes les sockets qui se connectent :
io.sockets.on('connection',(socket) =>{   //Si il recoit un connection de type socket, faire :
                                          //le "socket" est "l'id" de connection de client
   //Quand quelqu'un se connecte :
   io.emit('serverTell','Someone joined !');     //On dit à tout le monde que quelqu'un a rejoint
   socket.emit('serverTell','Welcome to you !'); // On dit à cette personne en particulier bienvenue
   moduleScore.exportDataFromFile(pathToScoreboard,socket)


   console.log("Player connected :", socket.id)    //et sur la console server, son id de connection


  //Ce .on sert à manager le chat
   socket.on('chatSend',(from,msg) => {
      io.emit('chatReceive',from,msg);
   });

   //Appelé quand le joueur recherche un partie, donnant son nom
   // et son niveau
   socket.on('playerSearching',(playerName, skillRating,isSearching) =>{
      if(isSearching){ //Si il active la recherche de game

         //On vient instancier un nouveau joueur
         let player = new PlayerServerSide(playerName, skillRating, socket);
         player.isSearching = true;

         allPlayersInQueue.printSearchInfo(playerName,skillRating);

         //On l'ajoute à la queue
         allPlayersInQueue.addInWaitQueue(player)
         socket.emit("serverTell","You are in queue");

         //On vient chercher le joueur adverse
         let matchup = allPlayersInQueue.matchmakingSSO(player);

         if (matchup !== undefined){ //si il a été trouvé

            socket.emit('onMatchFound',"Game found ! Joining...")
            matchup.socket.emit('onMatchFound',"Game found ! Joining...")

            //On enlève les deux joueurs de la file d'attente
            allPlayersInQueue.removeFromWaitQueue(socket.id);
            allPlayersInQueue.removeFromWaitQueue(matchup.socket.id);

            //On mets à jour leurs attributs
            player.isInGame = true;
            player.isSearching = false;
            matchup.isInGame = true;
            matchup.isSearching = false;

            console.log("Match : ", matchup.name," vs ", player.name," game successfully created !")

            //On les envoie sur la page de jeu
            socket.emit("SendClientToGame");
            matchup.socket.emit("SendClientToGame")

            //On créer une salle de jeu avec ces joueurs
            let room = new GameRoomServerSide();

            room.setPlayer1(player);
            room.setPlayer2(matchup);

            allRooms.addNewRoom(room)

         }
         else{
            socket.emit('serverTell','Not enough player, longer queue time expected')
         }
      }
      else{ //sinon (si il cancel la recherche)
         allPlayersInQueue.removeFromWaitQueue(socket.id)
         delete player
         socket.emit("serverTell","You are no longer queued");
      }





   });



   /*-----------------------------------------------------------------------*/
   /*---------------------------Partie clientInGame.js-----------------------*/
   /*-----------------------------------------------------------------------*/

   //Ce .on est appelé dès lorqu'un joueur arrive sur la page game.html
   //Et on vient réassigner son socket (car on ne peut pas garder
   //le même socket d'une page à l'autre)


   socket.on('reassignSockets',()=>{

      let room = allRooms.getLastestRoom()

      if (room == undefined){
         socket.emit('SendClientToMainMenu')
      }
      //Si il y a 2 joueurs dans la dernière room créée
      //Et si la partie n'est pas encore lancé
      else if( (room.player1 !== undefined && room.player2 !== undefined)   &&  room.isRunning == false ){
         //Si le joueur1 n'a pas encore de socket
         if(room.player1.socket.disconnected == true){
            room.player1.setNewSocket(socket)
         }
         //Si le joueur 2 n'a pas encore de socket
         else if(room.player2.socket.disconnected == true){
            room.player2.setNewSocket(socket)


            //On peut lancer la partie car le deuxième joueur est connecté
            console.log("\n\n\n\n")

            room.isRunning = true;
            room.game = new Engine();
            initGame(room)

         }
      }
      // Sinon (le joueur s'est connecté à game.html sans y avoir été invité
      else{
         socket.emit('SendClientToMainMenu')
      }
   });



   socket.on('GAME_CLIENTtoSERV_sendMoveMade',(oldPos,newPos)=>{

      room = allRooms.getRoomWithSocketID(socket.id)
      console.log("NEW ROUND")
      round(room,oldPos,newPos);

   });


   function initGame(room){

      room.player1.socket.emit("GAME_SERVtoCLIENT_initGame",true,room.player1.playerName,room.player1.skillRating) //On set le joueur 1 comme étant blanc
      room.player2.socket.emit("GAME_SERVtoCLIENT_initGame",false,room.player2.playerName,room.player2.skillRating) //On set le joueur 1 comme étant blanc
      room.player1.socket.emit("GAME_SERVtoCLIENT_sendWhoCanPlay","white")
      room.player2.socket.emit("GAME_SERVtoCLIENT_sendWhoCanPlay","white")
      room.player1.socket.emit("GAME_SERVtoCLIENT_sendBoardToDisplay",room.game.getFen())
      room.player2.socket.emit("GAME_SERVtoCLIENT_sendBoardToDisplay",room.game.getFen())
      room.player1.socket.emit("GAME_SERVtoCLIENT_sendMoveList",room.game.getMovesList())

   }

   // Fonction appelé uniquement si le jeu n'est pas fini.
   // (si le jeu est fini, et qu'on appelle la fonciton, il ne se passera rien)
   // "oldPos" may be either the current position of a pawn or the desired promotion for a pawn.
   function round(room, oldPos, newPos = undefined) {

      engine = room.game
      // If there's no pawn to promote
      if (!engine.isPromotionAvailable()) {

         //console.log(`engine.setUserMove(${oldPos}, ${newPos})`)
         engine.setUserMove(oldPos, newPos)

         //console.log(`It is to --==[ ${engine.chessboard.actualPlayer} ]==-- player to play`) // Affiche à qui c'est le tour





         // Test if the game is finished or not, if so, what kind of game over
         switch(engine.isGameOver()) {
            case false:
               room.player1.socket.emit("GAME_SERVtoCLIENT_sendWhoCanPlay",engine.chessboard.actualPlayer)
               room.player2.socket.emit("GAME_SERVtoCLIENT_sendWhoCanPlay",engine.chessboard.actualPlayer)
               //SI c'est au joueur blanc de jouer
               if(engine.chessboard.actualPlayer == "white"){room.player1.socket.emit("GAME_SERVtoCLIENT_sendMoveList",room.game.getMovesList());}
               else{room.player2.socket.emit("GAME_SERVtoCLIENT_sendMoveList",room.game.getMovesList());}
               console.log(`The game is not over.`)
               break

            case undefined:
               room.player1.socket.emit("GAME_SERVtoCLIENT_sendWhoCanPlay","NA")
               room.player2.socket.emit("GAME_SERVtoCLIENT_sendWhoCanPlay","NA")
               room.player1.socket.emit("GAME_SERVtoCLIENT_sendGameOver",true,engine.chessboard.actualPlayer)
               room.player2.socket.emit("GAME_SERVtoCLIENT_sendGameOver",true,engine.chessboard.actualPlayer)
               setTimeout(function(room){room.player1.socket.emit("SendClientToMainMenu")},15000,room)
               setTimeout(function(room){room.player2.socket.emit("SendClientToMainMenu")},15000,room)
               console.log(`${engine.chessboard.opponentColor(engine.chessboard.actualPlayer)} is in stalemate`)
               break

            default:
               room.player1.socket.emit("GAME_SERVtoCLIENT_sendWhoCanPlay","NA")
               room.player2.socket.emit("GAME_SERVtoCLIENT_sendWhoCanPlay","NA")
               room.player1.socket.emit("GAME_SERVtoCLIENT_sendGameOver",false,engine.chessboard.isGameOver())
               room.player2.socket.emit("GAME_SERVtoCLIENT_sendGameOver",false,engine.chessboard.isGameOver())

               let score = moduleScore.calculateScore(room.player1,room.player2,engine.chessboard.isGameOver(),engine.getFen())
               moduleScore.saveDataToFile(pathToScoreboard,score)

               setTimeout(function(room){room.player1.socket.emit("SendClientToMainMenu")},15000,room)
               setTimeout(function(room){room.player2.socket.emit("SendClientToMainMenu")},15000,room)

               console.log(`${engine.chessboard.isGameOver()} player win the game !`)
               break
         }

         // Tests whether after the move, a promotion is available.
         if (engine.isPromotionAvailable())
            if(engine.chessboard.actualPlayer == "white"){room.player1.socket.emit("GAME_SERVtoCLIENT_sendPromotionAvailable");}
            else{room.player2.socket.emit("GAME_SERVtoCLIENT_sendPromotionAvailable");}

            console.log(`A promotion is available, call round(your choice) between { '', 'q', 'r', 'n', 'b' }`)
     }
     // If there's a promotion to be made
     else {
         if (newPos != undefined) { // C'est que que oldPos == promotionWanted
            if (oldPos == parseInt(data, 10))
               engine.setPromotion(oldPos)
            else // Mauvais input => on informe de ce qu'on attend
               console.log(`A promotion is available, call round(your choice) between { '', 'q', 'r', 'n', 'b' }`)
         }
         else // Otherwise, we'll send the promotion (As a reminder, oldPos is here a promotion).
            engine.chessboard.promote(oldPos)
      }

     //On envoie aux joueurs le terrain de jeu
     room.player1.socket.emit("GAME_SERVtoCLIENT_sendBoardToDisplay", engine.getFen(), engine.getHistory())
     room.player2.socket.emit("GAME_SERVtoCLIENT_sendBoardToDisplay", engine.getFen(), engine.getHistory())

   }





});


server.listen(port);
console.log('Serveur lancé !');
